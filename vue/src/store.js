import Vue from 'vue'
import Vuex from 'vuex'
import Process from './models/process.js'
import axios from 'axios'
import Api from './api.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    process: new Process(),
    samplingColumns: [],
    samplingRows: [],
    processList: [],
  },
  mutations: {
    SET_PROCESS(state, process) {
      state.process = process
    },
    SET_PROCESSES(state, processes) {
      state.processList = processes
    },
    SET_SAMPLING(state, samplingData) {
      state.samplingColumns = samplingData.columns
      state.samplingRows = samplingData.rows
    }
  },
  actions: {
    init({ commit }) {
      commit('SET_PROCESS', new Process())
    },
    loadProcess({ commit }, id) {
      Api.getProcess(id)
        .then(response => (commit('SET_PROCESS', response.data)))
    },
    loadProcesses({ commit }) {
      axios
        .get('https://localhost:5001/api/process/')
        .then(response => (commit('SET_PROCESSES', response.data)))
    },
    loadSamplingData({ commit }, id) {

      axios
        .get('https://localhost:5001/api/measures/process/' + id + "/samplingcolumns")
        .then(response => {
          let columns = response.data;
          axios
            .get('https://localhost:5001/api/measures/process/' + id + "/samplingdata")
            .then(response => {
              let rows = response.data;
              commit('SET_SAMPLING', { columns, rows })
            })
        })

    },
    saveSamplingData({ commit, dispatch }) {
      /*id = options.id
      rows = options.rows
      updatedRows = []
      addedRows = []
      rows.forEach(row => {
        row.id === null ? addedRows.push(row) : updatedRows.push(row)
      })*/

      this.state.process.samplingRows = this.state.samplingRows
      let id = this.state.process.id

      axios.post('https://localhost:5001/api/measures/process/' + this.state.process.id + "/samplingdata", this.state.process.samplingRows)
        .then(function (response) {
          dispatch("loadSamplingData", id)
        })


      //call update and create methods from API

    },
    async createProcess({ commit, dispatch }) {
      let response = await Api.createProcess(this.state.process);
      dispatch("loadProcess", response.data.id);
      return response.data.id;
    },
    updateProcess({ commit, dispatch }) {
      Api.updateProcess(this.state.process)
    },
    uploadMonitoringData({ commit }, measures) {
      var measureType
      var data = []
      for (measureType in measures) {
        var uploadData = {
          measureType: {
            name: measureType
          },
          measures: measures[measureType]
        }
        data.push(uploadData)
        console.log(uploadData)

      }
      axios.post('https://localhost:5001/api/process/' + this.state.process.id + "/monitoringdata", data)
        .then(function (response) {
          console.log("uploaded")
        })
    }
  }
})
