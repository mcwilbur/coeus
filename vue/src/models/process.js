import moment from 'moment'
export default class Process {
    constructor(options) {
        if(options === undefined) options = {}
        this.id = options.id != undefined ? options.id : 0
        this.name = options.name != undefined ? options.name : "";
        this.innoculationTime = options.innoculationTime != undefined ? options.innoculationTime : moment().format('YYYY-MM-DD[T]hh:mm:ss'); // July 4th 2019, 10:58:26 pm
        this.description = options.description != undefined ? options.description : ""
        this.objectives = options.objectives != undefined ? options.objectives : ""
        if(options.product === undefined) options.product = {}
        this.product = {
            name:options.product.name != undefined ? options.product.name : ""
        }
        if(options.reactor === undefined) options.reactor = {}
        this.reactor = {
            name:options.reactor.name != undefined ? options.reactor.name : ""
        }
        if(options.strain === undefined) options.strain = {}
        this.strain = {
            name:options.strain.name != undefined ? options.strain.name : ""
        }
        if(options.medium === undefined) options.medium = {}
        this.medium = {
            composition:options.medium.composition != undefined ? options.medium.composition : "",
            preparation:options.medium.preparation != undefined ? options.medium.preparation : ""
        }
    }
    getCopy() {
        return new Process({
            id: this.id,
            name: this.name,
            innoculationTime: this.innoculationTime,
            product: this.product,
            reactor: this.reactor,
            strain: this.strain,
            description: this.description,
            objectives: this.objectives,
            medium: {
                composition: this.medium.composition,
                preparation: this.medium.preparation
            }
        })
    }
}