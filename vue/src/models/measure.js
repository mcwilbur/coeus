export default class Measure {
    constructor(options) {
        if(options === undefined) options = {}
        this.id = options.id != undefined ? options.id : null
        this.name = options.name != undefined ? options.name : null;
        this.innoculationTime = options.innoculationTime != undefined ? options.innoculationTime : null
        this.product = options.product != undefined ? options.product : null
        this.reactor = options.reactor != undefined ? options.reactor : null
        this.strain = options.strain != undefined ? options.strain : null
        this.description = options.description != undefined ? options.description : null
        this.objectives = options.objectives != undefined ? options.objectives : null
        if(options.medium === undefined) options.medium = {}
        this.medium = {
            composition:options.medium.composition != undefined ? options.medium.composition : null,
            preparation:options.medium.preparation != undefined ? options.medium.preparation : null
        }
    }
    getCopy() {
        return new Process({
            id: this.id,
            name: this.name,
            innoculationTime: this.innoculationTime,
            product: this.product,
            reactor: this.reactor,
            strain: this.strain,
            description: this.description,
            objectives: this.objectives,
            medium: {
                composition: this.medium.composition,
                preparation: this.medium.preparation
            }
        })
    }
}