import axios from 'axios'


export default {
    async getProcess(id) {
        return await axios
            .get('https://localhost:5001/api/process/' + id)
    },

    loadProcesses({ commit }) {
        axios
            .get('https://localhost:5001/api/process/')
            .then(response => (commit('SET_PROCESSES', response.data)))
    },

    async getReactors() {
        return await axios
            .get('https://localhost:5001/api/process/properties/reactor')
    },
    async getStrains() {
        return await axios
            .get('https://localhost:5001/api/process/properties/strain')
    },
    async getProducts() {
        return await axios
            .get('https://localhost:5001/api/process/properties/product')
    },
    async getMediums() {
        return await axios
            .get('https://localhost:5001/api/process/properties/medium')
    },
    async getMeasureTypes() {
        return await axios
            .get('https://localhost:5001/api/process/properties/measuretype')
    },
    async createProcess(process) {
        return await axios.post('https://localhost:5001/api/process/', process)
    },
    async updateProcess(process) {
        return await axios.put('https://localhost:5001/api/process/' + process.id, process)
    },
    async addSamplingMeasureTypes(options) {
        return await axios.post('https://localhost:5001/api/measures/process/' + options.id + '/samplingmeasuretype', options.measureTypes)
    },
    async getMeasures(options) {
        return await axios.get('https://localhost:5001/api/measures/process/' + options.processId + '/measuretype/' + options.measureTypeId)
    },
    async exportSampling(id) {
        const FileDownload = require('js-file-download');

        axios.get('https://localhost:5001/api/measures/process/' + id + '/export')
            .then((response) => {
                let headerLine = response.headers['content-disposition']
                let startFileNameIndex = headerLine.indexOf('"') + 1
                let endFileNameIndex = headerLine.lastIndexOf('"')
                let filename = headerLine.substring(startFileNameIndex, endFileNameIndex)
                FileDownload(response.data, filename);
            });
    },

    loadSamplingData({ commit }, id) {

        axios
            .get('https://localhost:5001/api/measures/process/' + id + "/samplingcolumns")
            .then(response => {
                let columns = response.data;
                axios
                    .get('https://localhost:5001/api/measures/process/' + id + "/samplingdata")
                    .then(response => {
                        let rows = response.data;
                        commit('SET_SAMPLING', { columns, rows })
                    })
            })

    },
    saveSamplingData({ commit, dispatch }) {

        this.state.process.samplingRows = this.state.samplingRows
        let id = this.state.process.id

        axios.post('https://localhost:5001/api/measures/process/' + this.state.process.id + "/samplingdata", this.state.process.samplingRows)
            .then(function (response) {
                dispatch("loadSamplingData", id)
            })


        //call update and create methods from API

    },
    uploadMonitoringData({ commit }, measures) {
        var measureType
        var data = []
        for (measureType in measures) {
            var uploadData = {
                measureType: {
                    name: measureType
                },
                measures: measures[measureType]
            }
            data.push(uploadData)
            console.log(uploadData)

        }
        axios.post('https://localhost:5001/api/process/' + this.state.process.id + "/monitoringdata", data)
            .then(function (response) {
                console.log("uploaded")
            })
    }
}
