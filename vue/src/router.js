import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: '/process/list'
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/process/new',
      name: "newProcess",
      component: () => import('./views/Process/Create'),
    },
    {
      path: '/process/list',
      name: 'processList',
      component: () => import('./views/Process/List'),
    },
    {
      path: '/process/:id/update',
      props: true,
      name: 'update',
      component: () => import('./views/Process/Update.vue')
    },
    {
      path: '/process/:id',
      props: true,
      component: () => import('./components/Process/Container'),
      children: [
        {
          path: '',
          name: 'process',
          redirect: 'info'

        },
        {
          path: 'info',
          name: 'info',
          component: () => import('./views/Process/Info.vue')
        },
        {
          path: 'medium',
          name: 'medium',
          component: () => import('./views/Process/Medium.vue')
        },
        {
          path: 'settings',
          name: 'settings',
          component: () => import('./views/Process/Settings.vue')
        },
        {
          path: 'sampling',
          name: 'sampling',
          component: () => import('./views/Process/Sampling.vue')
        },
        {
          path: 'upload',
          name: 'upload',
          component: () => import('./views/Process/Upload.vue')
        },
        {
          path: 'charts',
          name: 'charts',
          component: () => import('./views/Process/Charts.vue')
        },
      ]
    },
  ]
})
