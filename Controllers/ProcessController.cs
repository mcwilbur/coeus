using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coeus.Models;

namespace Coeus.Controllers
{
    [Route("api/process")]
    [ApiController]
    public class ProcessController : ControllerBase
    {
        private readonly AppDBContext _context;

        public ProcessController(AppDBContext context)
        {
            _context = context;
        }

        // GET: api/process
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Process>>> GetProcesses()
        {
            return await _context.Processes
            .Include(p => p.Medium)
            .Include(p => p.Product)
            .Include(p => p.Strain)
            .Include(p => p.Reactor)
            .ToListAsync();
        }

        // GET: api/process/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Process>> GetProcess(int id)
        {
            var process = await _context.Processes
            .Include(p => p.Medium)
            .Include(p => p.Product)
            .Include(p => p.Strain)
            .Include(p => p.Reactor)
            .SingleOrDefaultAsync(p => p.Id == id);

            if (process == null)
            {
                return NotFound();
            }

            return process;
        }

        // POST: api/process/
        [HttpPost]
        public async Task<ActionResult<Process>> PostProcess(Process process)
        {
            _context.Medium.Attach(process.Medium);
            _context.Products.Attach(process.Product);
            _context.Strains.Attach(process.Strain);
            _context.Reactors.Attach(process.Reactor);
            _context.Processes.Add(process);
            _context.SaveChanges();
            return process;
        }

        // POST: api/process/
        [HttpPut("{id}")]
        public async Task<ActionResult<Process>> PutProcess(int id, Process process)
        {
            _context.Medium.Attach(process.Medium);
            _context.Products.Attach(process.Product);
            _context.Strains.Attach(process.Strain);
            _context.Reactors.Attach(process.Reactor);
            _context.Update(process) ;
            _context.SaveChanges();
            return process;
        }
        
    }
}