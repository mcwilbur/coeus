using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coeus.Models;
using System.Text;

namespace Coeus.Controllers
{
    [Route("api/measures/process")]
    [ApiController]
    public class ProcessMeasuresController : ControllerBase
    {
        private readonly AppDBContext _context;

        public ProcessMeasuresController(AppDBContext context)
        {
            _context = context;
        }

        // GET: api/process/1/samplingcolumns
        [HttpGet("{id}/samplingcolumns")]
        public async Task<ActionResult<List<MeasureType>>> GetSamplingColumns(int id)
        {
            var process = await _context.Processes
            .Include(p => p.SamplingMeasureTypes)
            .ThenInclude(s => s.MeasureType)
            .ThenInclude(m => m.Unit)
            .SingleOrDefaultAsync(p => p.Id == id);

            var columns = new List<MeasureType>();
            foreach (var column in process.SamplingMeasureTypes)
            {
                columns.Add(column.MeasureType);
            }
            return columns;
        }

        [HttpGet("{id}/samplingdata")]
        public async Task<ActionResult<List<SamplingRow>>> GetSamplingData(int id)
        {
            var process = await _context.Processes
            .Include(p => p.SamplingRows)
            .ThenInclude(s => s.Measures)
            .ThenInclude(m => m.MeasureType)
            .SingleOrDefaultAsync(p => p.Id == id);

            return process.SamplingRows;
        }

        [HttpPost("{id}/samplingdata")]
        public async Task<ActionResult> PostSamplingData(int id, List<SamplingRow> samplingRows)
        {
            var process = await _context.Processes
            .Include(p => p.SamplingRows)
            .ThenInclude(s => s.Measures)
            .SingleOrDefaultAsync(p => p.Id == id);

            if (process == null)
            {
                return NotFound();
            }

            foreach (SamplingRow postedRow in samplingRows)
            {
                SamplingRow row = process.SamplingRows.Where(r => r.Id == postedRow.Id && r.Id != 0).FirstOrDefault();

                if (row != null)
                {
                    row.TimeStamp = postedRow.TimeStamp;
                    foreach (Measure measure in postedRow.Measures)
                    {
                        row.Measures.Where(m => m.Id == measure.Id).FirstOrDefault().Value = measure.Value;
                    }
                }
                else
                {
                    foreach (Measure measure in postedRow.Measures)
                    {
                        MeasureType measureType = _context.MeasureTypes.SingleOrDefault(m => m.Id == measure.MeasureType.Id);
                        measure.MeasureType = measureType;
                        measure.Timestamp = postedRow.TimeStamp;
                    }
                    process.SamplingRows.Add(postedRow);
                }
            }

            _context.Update(process);
            _context.SaveChanges();

            return Ok();
        }

        [HttpPost("{id}/monitoringdata")]
        public async Task<ActionResult> PostMonitoringData(int id, List<MonitoringDataVM> monitoringDatas)
        {
            //_context.ChangeTracker.AutoDetectChangesEnabled = false;

            Process process = await _context.Processes
            .Include(p => p.Measures)
            .Include(p => p.MeasureTypes)
            .ThenInclude(m => m.MeasureType)
            .SingleOrDefaultAsync(p => p.Id == id);

            foreach (MonitoringDataVM monitoringData in monitoringDatas)
            {

                MeasureType measureType = await _context.MeasureTypes.Where(m => m.Name == monitoringData.MeasureType.Name).FirstOrDefaultAsync();
                if (measureType == null)
                {
                    measureType = monitoringData.MeasureType;
                    _context.MeasureTypes.Add(measureType);
                }
                if (process.MeasureTypes.Where(m => m.MeasureType.Id == measureType.Id).FirstOrDefault() == null)
                {
                    Process_MeasureType measureTypeRelation = new Process_MeasureType();
                    measureTypeRelation.MeasureType = measureType;
                    process.MeasureTypes.Add(measureTypeRelation);
                }
                //_context.SaveChanges();
                foreach (Measure measure in monitoringData.Measures)
                {
                    //measure.MeasureTypeId = measureType.Id;
                    //measure.ProcessId = process.Id;
                    process.Measures.Add(measure);
                }
                process.Measures.AddRange(monitoringData.Measures);
                _context.SaveChanges();
            }
            //_context.Update(process);
            //_context.Measures.AddRange(process.Measures);
            //_context.ChangeTracker.DetectChanges();
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("{id}/samplingmeasuretype")]
        public async Task<ActionResult> PostSamplingMeasureTypes(int id, List<MeasureType> samplingMeasureTypes)
        {
            _context.AttachRange(samplingMeasureTypes);
            Process process = await _context.Processes
            .Include(p => p.SamplingMeasureTypes)
            .SingleOrDefaultAsync(p => p.Id == id);
            foreach (MeasureType measureType in samplingMeasureTypes)
            {
                process.SamplingMeasureTypes.Add(new Process_SamplingMeasureType() { MeasureType = measureType });
            }
            _context.SaveChanges();
            return Ok();
        }

        [HttpGet("{id}/measuretype/{measureTypeId}")]
        public ActionResult<List<Measure>> GetMeasures(int id, int measureTypeId)
        {
            List<Measure> measures = new List<Measure>();
            measures.Add(_context.Measures.Find(11));
            measures.Add(_context.Measures.Find(15));
            measures.Add(_context.Measures.Find(19));

            return Ok(measures);
        }

        [HttpGet("{id}/export")]
        public IActionResult CSV(int id)
        {
            var process = _context.Processes
            .Include(p => p.SamplingMeasureTypes).ThenInclude(s => s.MeasureType)
            .Include(p => p.SamplingRows).ThenInclude(s => s.Measures).ThenInclude(m => m.MeasureType)
            .FirstOrDefault(p => p.Id == id);
            var samplingcolumns = process.SamplingMeasureTypes;
            var comlumHeadrs = "timestamp";

            var filename = process.InnoculationTime.ToString("yyyy-MM-dd")+"_"+process.Name+".csv";

            foreach (var samplingcolumn in samplingcolumns)
            {
                comlumHeadrs += ";"+samplingcolumn.MeasureType.Name;
            }

            var lines = new List<string>();
            foreach (var samplingRow in process.SamplingRows)
            {
                var line = samplingRow.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss");
                foreach (var measureType in samplingcolumns)
                {
                    var measure = samplingRow.Measures.Where(m => m.MeasureType == measureType.MeasureType).FirstOrDefault();
                    line += ";"+measure.Value;
                }
                lines.Add(line);
            }


            // Build the file content
            var samplingcsv = new StringBuilder();
            lines.ForEach(line =>
                {
                    samplingcsv.AppendLine(string.Join(",", line));
                });

            byte[] buffer = Encoding.ASCII.GetBytes($"{string.Join(",", comlumHeadrs)}\r\n{samplingcsv.ToString()}");
            return File(buffer, "text/csv", filename);

        }

    }
}