using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Coeus.Models;

namespace Coeus.Controllers
{
    [Route("api/process/properties")]
    [ApiController]
    public class ProcessPropertiesController : ControllerBase
    {
        private readonly AppDBContext _context;

        public ProcessPropertiesController(AppDBContext context)
        {
            _context = context;
        }

        // GET: api/process/properties/reactor
        [HttpGet("reactor")]
        public async Task<ActionResult<IEnumerable<Reactor>>> GetReactors()
        {
            return await _context.Reactors
            .ToListAsync();
        }

        // GET: api/process/properties/medium
        [HttpGet("medium")]
        public async Task<ActionResult<IEnumerable<Medium>>> GetMediums()
        {
            return await _context.Medium
            .ToListAsync();
        }

        // GET: api/process/properties/strain
        [HttpGet("strain")]
        public async Task<ActionResult<IEnumerable<Strain>>> GetStrains()
        {
            return await _context.Strains
            .ToListAsync();
        }

        // GET: api/process/properties/product
        [HttpGet("product")]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts()
        {
            return await _context.Products
            .ToListAsync();
        }

        [HttpGet("measuretype")]
        public async Task<ActionResult<IEnumerable<MeasureType>>> GetMeasureTypes()
        {
            return await _context.MeasureTypes
            .ToListAsync();
        }

    }
}