
namespace Coeus.Models
{
    public class Process_SamplingMeasureType
    {
        public int ProcessId {get; set;}
        public Process Process { get; set; }
        public int MeasureTypeId {get; set;}
        public MeasureType MeasureType {get; set;}
    }
}