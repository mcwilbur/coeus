using System;
using System.Collections.Generic;

namespace Coeus.Models
{
    public class SamplingRow
    {
        public int Id {get;set;}
        public int RowNumber {get; set;}
        public DateTime TimeStamp {get; set;}
        public List<Measure> Measures {get; set;}
    }
}