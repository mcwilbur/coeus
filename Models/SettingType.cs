namespace Coeus.Models
{
    public class SettingType
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public Unit Unit {get; set;}
    }
}