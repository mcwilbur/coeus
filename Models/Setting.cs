using System;
namespace Coeus.Models
{
    public class Setting
    {
        public int Id {get; set;}
        public DateTime Timestamp {get; set;}
        public double Value {get; set;}
        public SettingType SettingType {get; set;}
    }
}