namespace Coeus.Models
{
    public class Strain
    {
        public int Id {get; set;}
        public string Name {get; set;}
    }
}