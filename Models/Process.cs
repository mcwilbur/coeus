using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Coeus.Models
{
    public class Process
    {
        public int Id {get; set;}
        public string Name {get; set;}
        public DateTime InnoculationTime {get; set;}
        public Product Product {get; set;}
        public Reactor Reactor{get; set;}
        public Strain Strain{get; set;}
        public string Description{get; set;}
        public string Objectives{get; set;}
        public Medium Medium {get; set;}
        public List<Process_MeasureType> MeasureTypes {get; set;}
        public List<Measure> Measures {get; set;}
        public List<Process_SamplingMeasureType> SamplingMeasureTypes {get; set;}
        public List<Process_SettingType> SettingTypes {get; set;}
        public List<Setting> Settings {get; set;}
        public List<SamplingRow> SamplingRows {get; set;}
    }
}