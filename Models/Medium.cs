namespace Coeus.Models
{
    public class Medium
    {
        public int Id {get; set;}
        public string Composition {get; set;}
        public string Preparation {get; set;}
    }
}