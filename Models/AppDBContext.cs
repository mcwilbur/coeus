using System.Security.Cryptography;
using Microsoft.EntityFrameworkCore;

namespace Coeus.Models
{
    public class AppDBContext : DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {
        }

        public DbSet<Process> Processes { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<MeasureType> MeasureTypes { get; set; }
        public DbSet<Medium> Medium { get; set; }
        public DbSet<Setting> Setting { get; set; }
        public DbSet<SettingType> SettingType { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Strain> Strains { get; set; }
        public DbSet<Reactor> Reactors { get; set; }
        public DbSet<SamplingRow> SamplingRows { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Process_MeasureType>()
                .HasKey(pm => new { pm.ProcessId, pm.MeasureTypeId });

            modelBuilder.Entity<Process_MeasureType>()
                .HasOne(pm => pm.Process)
                .WithMany(p => p.MeasureTypes)
                .HasForeignKey(pm => pm.ProcessId);

            modelBuilder.Entity<Process_MeasureType>()
                .HasOne(pm => pm.MeasureType)
                .WithMany()
                .HasForeignKey(pm => pm.MeasureTypeId);


            
            modelBuilder.Entity<Process_SamplingMeasureType>()
                .HasKey(psm => new { psm.ProcessId, psm.MeasureTypeId });

            modelBuilder.Entity<Process_SamplingMeasureType>()
                .HasOne(psm => psm.Process)
                .WithMany(p => p.SamplingMeasureTypes)
                .HasForeignKey(psm => psm.ProcessId);

            modelBuilder.Entity<Process_SamplingMeasureType>()
                .HasOne(psm => psm.MeasureType)
                .WithMany()
                .HasForeignKey(psm => psm.MeasureTypeId);



            modelBuilder.Entity<Process_SettingType>()
                .HasKey(ps => new { ps.ProcessId, ps.SettingTypeId });

            modelBuilder.Entity<Process_SettingType>()
                .HasOne(ps => ps.Process)
                .WithMany(p => p.SettingTypes)
                .HasForeignKey(ps => ps.ProcessId);

            modelBuilder.Entity<Process_SettingType>()
                .HasOne(ps => ps.SettingType)
                .WithMany()
                .HasForeignKey(ps => ps.SettingTypeId);
        }
    }
}