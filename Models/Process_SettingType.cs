namespace Coeus.Models
{
    public class Process_SettingType
    {
        public int ProcessId {get; set;}
        public Process Process { get; set; }
        public int SettingTypeId {get; set;}
        public SettingType SettingType {get; set;}
    }
}