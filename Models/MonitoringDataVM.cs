using System.Collections.Generic;
namespace Coeus.Models
{
    public class MonitoringDataVM
    {
        public MeasureType MeasureType {get; set;}
        public List<Measure> Measures {get; set;}
    }
}