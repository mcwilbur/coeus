using System;
namespace Coeus.Models
{
    public class Measure
    {
        public int Id {get; set;}
        public DateTime Timestamp {get; set;}
        public double Value {get; set;}
        public MeasureType MeasureType {get; set;}
        
    }
}